---
author: bozsikarmand
comments: true
date: 2015-09-06 09:00:56+00:00
layout: post
link: https://blog.bozsikarmand.hu/2015/09/06/a-battlefield-hardline-probaverzioja-elerhetove-valt-xbox-one-ra-az-ea-access-szolgaltatas-kereteben/
slug: a-battlefield-hardline-probaverzioja-elerhetove-valt-xbox-one-ra-az-ea-access-szolgaltatas-kereteben
title: A Battlefield Hardline próbaverziója elérhetővé vált Xbox One-ra, az EA Access
  szolgáltatás keretében
wordpress_id: 211
categories:
- Hungarian
---

Az EA Access előfizetéssel rendelkező Xbox One tulajdonosok számára a mai naptól elérhetővé vált a Battlefield Hardline ingyenes előzetese. Alább olvashatod a hivatalos Twitter bejelentés szövegét:


<blockquote>The [#BFHardline](https://twitter.com/hashtag/BFHardline?src=hash) trial is now live! Start your story or hit the streets in MP. Download it now from the EA Access Hub. [pic.twitter.com/SwXzCn4ZcP](http://t.co/SwXzCn4ZcP)

— EA Access (@EAAccess) [March 12, 2015](https://twitter.com/EAAccess/status/575835457646411777)</blockquote>




Amennyiben élünk a felkínált lehetőséggel, és letöltjük a programot, betekintést nyerhetünk a március 17-én érkező interaktív rabló pandúr harcba.

A kipróbálható verzió, melynek mérete igencsak tetemes, – egészen pontosan 45GB - 10 óra játékidőt kínál. A kipróbálási lehetőség alatt a többjátékos mód teljes egészében elérhető, az egyjátékosból viszont csak a prológus illetve az első küldetés játszható.

Azok a játékosok, akik megvásárolják a teljes verziót, továbbvihetik a próbaverzióban megszerzett javaikat a végleges változatba is, illetve a jelenlegi EA Access előfizetők a Battlefield Hardline digitálisan terjesztett verzióját 10% kedvezménnyel vásárolhatják meg.

Az EA Access jelenleg havi 990 Ft-ba kerül Xbox One-ra, ami a kedvezmények mellett 9 teljes verziójú EA játék használatára is feljogosít.

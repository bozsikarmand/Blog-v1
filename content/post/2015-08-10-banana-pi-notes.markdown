---
author: bozsikarmand
comments: true
date: 2015-08-10 11:45:19+00:00
layout: post
link: https://blog.bozsikarmand.hu/2015/08/10/banana-pi-notes/
slug: banana-pi-notes
title: Banana Pi notes
wordpress_id: 129
categories:
- Home server
tags:
- banana
- banana pi
- pi
---

[Specification](http://www.bananapi.org/p/product.html)

Install OSes:



	
  * [Linux](https://blog.bozsikarmand.hu/2015/08/10/install-ubuntu-on-banana-pi/)

	
  * [Android](https://blog.bozsikarmand.hu/2015/08/10/install-android-on-banana-pi/)




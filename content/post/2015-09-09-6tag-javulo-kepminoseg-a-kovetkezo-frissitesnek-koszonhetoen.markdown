---
author: bozsikarmand
comments: true
date: 2015-09-09 15:59:45+00:00
layout: post
link: https://blog.bozsikarmand.hu/2015/09/09/6tag-javulo-kepminoseg-a-kovetkezo-frissitesnek-koszonhetoen/
slug: 6tag-javulo-kepminoseg-a-kovetkezo-frissitesnek-koszonhetoen
title: '6tag: Javuló képminőség a következő frissítésnek köszönhetően'
wordpress_id: 257
categories:
- Hungarian
---

## Tegnap este a 6tag (és számos egyéb alkalmazás) fejlesztője, Rudy Huyn egy képet tett közzé Twitter csatornáján, hogy érzékeltesse az elkövetkező, 4.1-es frissítésben várható képminőség javulást




<blockquote>6tag 4.1 will dramatically improve image quality! See samples here! [http://t.co/exgwL0ONjY](http://t.co/exgwL0ONjY) [#windowsphone](https://twitter.com/hashtag/windowsphone?src=hash) [#wpdev](https://twitter.com/hashtag/wpdev?src=hash)

— Rudy Huyn (@RudyHuyn) [March 31, 2015](https://twitter.com/RudyHuyn/status/582728497858252800)</blockquote>




A népszerű Instagram alkalmazás a jelenleg 4.0.6-os verzióhoz képest jelentős, főként a képminőséget érintő javuláson fog átesni, a megosztott kép alapján pedig a hivatalos, béta státuszú applikációhoz nem is lehet majd hasonlítani a változás mértékét.

[![6tag-41-improvements](https://blog.bozsikarmand.hu/wp-content/uploads/2015/09/6tag-41-improvements.jpg)](https://blog.bozsikarmand.hu/wp-content/uploads/2015/09/6tag-41-improvements.jpg)

A fenti képen a 6tag 4.0.6-os verziójának feldolgozási teljesítményét láthatjuk, összehasonlítva a 4.1-es verzióval. Igen csekélynek tűnik a különbség, de amit rögtön észrevehetünk az a tisztább kép, és a nagyobb képélesség, melyre a legjobb példa a képen látható ág.

Huyn nem közölte, hogy mikor kívánja megjelentetni a 6tag frissítését, így nem tudjuk megmondani mikor lesz elérhető a felhasználók számára, de reménykedjünk, már nem lehet olyan messze. Amennyiben kevésbé tudod kivenni a képek közti különbségeket, [kattints ide](http://feelmygeek.com/6tagCompare/), a nagyított verziókért.

A 6tag-et letöltheted az alábbi gombra kattintva:

[gpp_button color="yellow" url="https://www.windowsphone.com/s?appId=7d795cdf-fb1b-4bdf-8f5e-76eb19f7079e" title="6tag" icon_left="cloud-download" target="_blank" size="large" display="block"]A 6tag letöltése Windows Phone-ra[/gpp_button]

Illetve az alábbi QR-kód beolvasásval:

[![QR_6Tag](https://blog.bozsikarmand.hu/wp-content/uploads/2015/09/QR_6Tag.png)](https://blog.bozsikarmand.hu/wp-content/uploads/2015/09/QR_6Tag.png)

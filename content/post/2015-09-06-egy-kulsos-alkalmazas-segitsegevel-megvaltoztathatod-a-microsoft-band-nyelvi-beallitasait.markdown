---
author: bozsikarmand
comments: true
date: 2015-09-06 08:58:36+00:00
layout: post
link: https://blog.bozsikarmand.hu/2015/09/06/egy-kulsos-alkalmazas-segitsegevel-megvaltoztathatod-a-microsoft-band-nyelvi-beallitasait/
slug: egy-kulsos-alkalmazas-segitsegevel-megvaltoztathatod-a-microsoft-band-nyelvi-beallitasait
title: Egy külsős alkalmazás segítségével megváltoztathatod a Microsoft Band nyelvi
  beállításait
wordpress_id: 208
categories:
- Hungarian
---

Amennyiben nem az Államokban élsz, és az anyanyelveden szeretnéd használni a Microsoft Bandedet (amit esetleg az eBay-ről rendeltél), jó híreink vannak számodra. A jelenleg 1.8-as verziónál járó Pimp My Band (sok kisebb frissítés után) ebben is segíthet a lapkák, színek, színsémák testreszabása mellett.

A mai frissítéssel végre a felhasználói felület nyelvét is megváltoztathatod (angol, spanyol, olasz, német és francia nyelveket támogat jelenleg az alkalmazás). A beállítás a néhány hete bemutatott “finomhangolási” opciók környékén található.

[![pimpband-langues](https://blog.bozsikarmand.hu/wp-content/uploads/2015/09/pimpband-langues1.png)](https://blog.bozsikarmand.hu/wp-content/uploads/2015/09/pimpband-langues1.png)

A Microsoft eddig nem nyilatkozott a nemzetközi piacot érintő terveiről, de úgy tűnik, nem igazán akarnak a Microsoft Banddel más régiók felé terjeszkedni jelenleg. Az Államok vásárlóközönsége nagyon jó tesztalanynak bizonyult eddig, és ez az ottani tapasztalatok szükségesek ahhoz, hogy a későbbiekben szélesebb tömegek számára is elérhetővé váljon a termék, mint ahogy erre Joe Belfiore utalt is októberben:


<blockquote>Folks asking about [#MSBand](https://twitter.com/hashtag/MSBand?src=hash) - We’re starting out in the US to learn and get feedback for a little while. Definitely stay tuned!

— joebelfiore (@joebelfiore) [October 30, 2014](https://twitter.com/joebelfiore/status/527848794340077568)</blockquote>




Azok pedig, akik nem bírnak várni, hogy megkaparinthassák ezt az igen népszerű eszközt, két lehetőség közül választhatnak: vagy egy az Államokban élő ismerősük segítségét veszik igénybe, vagy az erre szakosodott oldalakon licitálnak. Akárhogy is legyen, most már legalább szélesebb körű nyelvi támogatás reményével szerezhetik be a Bandet.

Általában a Pimp My Band 99 centbe szokott kerülni, de jelenleg ingyenesen elérhető a Store-ban.

[gpp_button color="yellow" url="http://www.windowsphone.com/s?appId=9966b6e7-bb20-4b53-9b3c-8701a271c66c" title="pimpmyband" icon_left="cloud-download" target="_blank" size="large" display="block"]A Pimp My Band letöltése Windows Phone-ra[/gpp_button]

[![QR-Pimp-My-Band](https://blog.bozsikarmand.hu/wp-content/uploads/2015/09/QR-Pimp-My-Band1.png)](https://blog.bozsikarmand.hu/wp-content/uploads/2015/09/QR-Pimp-My-Band1.png)
